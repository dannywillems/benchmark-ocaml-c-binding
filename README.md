# MISC benchmarks of C low-level primitives

```
opam switch create ./ 4.12.0 --empty
opam install merlin core_bench core ocamlformat.0.18.0
```

## Uint8 array custom block

Benchmarking memset, memcpy, allocation in a custom block.

Results on MacOS Big Sur, Intel(R) Core(TM) i9-9880H CPU @ 2.30GHz.

```
dune exec uint8_array/uint8_array.exe -- -quota 2s
Estimated testing time 24s (12 benchmarks x 2s). Change using '-quota'.
┌────────────────────────────────────┬─────────────┬─────────┬───────────┬────────────┐
│ Name                               │    Time/Run │ mWd/Run │  mjWd/Run │ Percentage │
├────────────────────────────────────┼─────────────┼─────────┼───────────┼────────────┤
│ Allocate uint8 of size 256         │     31.52ns │  34.00w │           │      0.14% │
│ Allocate uint8 of size 1024        │     24.65ns │ 130.00w │           │      0.11% │
│ Allocate uint8 of size 4096        │  1_539.22ns │         │   514.00w │      7.05% │
│ Allocate uint8 of size 65536       │ 21_832.26ns │         │ 8_194.01w │    100.00% │
│ Memcpy uint8 array with size 256   │      9.37ns │         │           │      0.04% │
│ Memcpy uint8 array with size 1024  │     15.01ns │         │           │      0.07% │
│ Memcpy uint8 array with size 4096  │     40.79ns │         │           │      0.19% │
│ Memcpy uint8 array with size 65536 │  1_538.56ns │         │           │      7.05% │
│ Memset uint8 array with size 256   │      8.77ns │         │           │      0.04% │
│ Memset uint8 array with size 1024  │     13.46ns │         │           │      0.06% │
│ Memset uint8 array with size 4096  │     36.34ns │         │           │      0.17% │
│ Memset uint8 array with size 65536 │  1_256.57ns │         │           │      5.76% │
└────────────────────────────────────┴─────────────┴─────────┴───────────┴────────────┘
```
