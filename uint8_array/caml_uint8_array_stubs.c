#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef uint8_t byte;

#define Uint8_array_val(v) ((uint8_t *)Data_custom_val(v))

static struct custom_operations uint8_ops = {"uint8_array",
                                             custom_finalize_default,
                                             custom_compare_default,
                                             custom_hash_default,
                                             custom_serialize_default,
                                             custom_deserialize_default,
                                             custom_compare_ext_default,
                                             custom_fixed_length_default};

CAMLprim value caml_allocate_uint8_array_stubs(value size) {
  CAMLparam1(size);
  CAMLlocal1(block);
  int size_c = Int_val(size);
  block = caml_alloc_custom(&uint8_ops, sizeof(uint8_t) * size_c, 0, 1);
  CAMLreturn(block);
}

CAMLprim value caml_uint8_array_memcpy_stubs(value dst, value src, value size) {
  CAMLparam3(dst, src, size);
  int size_c = Int_val(size);
  uint8_t *dst_c = Uint8_array_val(dst);
  uint8_t *src_c = Uint8_array_val(src);
  memcpy(dst_c, src_c, size_c * sizeof(uint8_t));
  CAMLreturn(Val_unit);
}

CAMLprim value caml_uint8_array_memset_stubs(value src, value size) {
  CAMLparam2(src, size);
  int size_c = Int_val(size);
  uint8_t *src_c = Uint8_array_val(src);
  memset(src_c, 0, size_c * sizeof(uint8_t));
  CAMLreturn(Val_unit);
}
