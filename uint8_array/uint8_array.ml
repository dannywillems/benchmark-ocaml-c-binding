open Core_bench

module Stubs = struct
  type uint8_array

  external allocate_uint8_array : int -> uint8_array
    = "caml_allocate_uint8_array_stubs"

  external memcpy : uint8_array -> uint8_array -> int -> unit
    = "caml_uint8_array_memcpy_stubs"

  external memset : uint8_array -> int -> unit = "caml_uint8_array_memset_stubs"
end

let test_allocate size =
  let name = Printf.sprintf "Allocate uint8 of size %d" size in
  Bench.Test.create ~name (fun () -> ignore (Stubs.allocate_uint8_array size))

let test_memcpy size =
  let src = Stubs.allocate_uint8_array size in
  let dst = Stubs.allocate_uint8_array size in
  let name = Printf.sprintf "Memcpy uint8 array with size %d" size in
  Bench.Test.create ~name (fun () -> ignore (Stubs.memcpy dst src size))

let test_memset size =
  let src = Stubs.allocate_uint8_array size in
  let name = Printf.sprintf "Memset uint8 array with size %d" size in
  Bench.Test.create ~name (fun () -> ignore (Stubs.memset src size))

let () =
  let commands =
    [ test_allocate (1 lsl 8);
      test_allocate (1 lsl 10);
      test_allocate (1 lsl 12);
      test_allocate (1 lsl 16);
      test_memcpy (1 lsl 8);
      test_memcpy (1 lsl 10);
      test_memcpy (1 lsl 12);
      test_memcpy (1 lsl 16);
      test_memset (1 lsl 8);
      test_memset (1 lsl 10);
      test_memset (1 lsl 12);
      test_memset (1 lsl 16) ]
  in
  Core.Command.run (Core_bench.Bench.make_command commands)
